#!/bin/sh
exec 2>&1
export PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
echo "<PRE>"

[ -z "$DOCUMENT_ROOT" ] && DOCUMENT_ROOT="./"
cd "$DOCUMENT_ROOT"

for freshclam_conf in /usr/local/etc/freshclam.conf /etc/clamav/freshclam.conf /etc/freshclam.conf ""; do
    [ -e "$freshclam_conf" ] && break
done
[ -z "$freshclam_conf" ] && echo "ERROR: freshclam.conf not found (make sure ClamAV is installed)" && exit 1

clamdb_dir=$(grep ^DatabaseDirectory $freshclam_conf | head -1 | awk '{print $2}')
clamdb_list="malware.expert.fp malware.expert.hdb malware.expert.ndb malware.expert.ldb malwarehash.hsb porcupine.hsb porcupine.ndb rfxn.hdb rfxn.ndb rogue.hdb ruweb.ndb ruweb.yar sanesecurity.ftm securiteinfo.hdb securiteinfohtml.hdb spamimg.hdb winnow.attachments.hdb winnow_bad_cw.hdb winnow_extended_malware.hdb winnow_malware.hdb"

perl -MMIME::Base64 -e 'print ""' || exit 1

[ -z "$clamdb_dir" ] && [ -e /usr/local/share/clamav/main.cvd -o -e /usr/local/share/clamav/main.cld ] && clamdb_dir=/usr/local/share/clamav
[ ! -d "$clamdb_dir" ] && echo "ERROR: ClamAV DatabaseDirectory '$clamdb_dir' not found (make sure DatabaseDirectory is specified inside $freshclam_conf)" && exit 1

clamscan=$(which clamscan)
[ -z "$clamscan" ] && echo "ERROR: clamscan not found" && exit 1

freshclam=$(which freshclam)
[ -z "$freshclam" ] && echo "WARNING: freshclam not found"

if hostname | grep -q '.deserv.net$'; then
    ! wget -O freshclam.conf http://mail.mensa.deserv.net/freshclam.conf && echo "Error downloading freshclam.conf" && exit 1
    [ -e /usr/local/etc/freshclam.conf ] && mv /usr/local/etc/freshclam.conf /usr/local/etc/freshclam.conf.`date +"%y%m%d%H%M%S"`
    cp -pv freshclam.conf /usr/local/etc/freshclam.conf
    ! grep -q clamscan /etc/crontab && echo "Adding freshclam && clamscan to /etc/crontab" && \
        echo "0 22 * * * root perl -le 'sleep rand 3600'; /usr/local/bin/freshclam --quiet --no-warnings 2>/dev/null || rm -f /var/db/clamav/mirrors.dat; /usr/local/bin/clamscan -irl /var/log/clamav/clamscan.log --quiet --no-summary --detect-pua=yes --remove=yes /tmp /var/tmp || date >> /var/log/clamav/clamscan.log" >> /etc/crontab
    if ! grep -q /sitescan_run.sh /var/cron/tabs/root; then
        perl -pi -e 's#/root/bin/zbackup#/root/bin/zbackup; /usr/local/directadmin/plugins/site_scan/scripts/sitescan_run.sh#' /var/cron/tabs/root
        cat /var/cron/tabs/root | grep -v "^# " | crontab -u root /dev/stdin
    fi
    /usr/local/bin/freshclam --update-db=main --no-warnings
else
    grep -q /sitescan_run.sh /etc/crontab \
        || echo -e "0 4 * * * root /usr/local/directadmin/plugins/site_scan/scripts/sitescan_run.sh #Added by Site Scan Plugin" >> /etc/crontab
fi

if [ ! -e "$DOCUMENT_ROOT/../clamav" ]; then
    mkdir -p "$DOCUMENT_ROOT/../clamav" || exit 1
    for file in main daily bytecode; do
        ln -s "$clamdb_dir/$file.cld" "$clamdb_dir/$file.cvd" "$DOCUMENT_ROOT/../clamav/"
    done
fi
for file in $clamdb_list $(cd "$clamdb_dir" && ls *.ign2 2>/dev/null); do
    [ -e "$clamdb_dir/$file" ] && [ ! -e "$DOCUMENT_ROOT/../clamav/$file" ] && ln -s "$clamdb_dir/$file" "$DOCUMENT_ROOT/../clamav/"
done


echo "Plugin Installed!"; #NOT! :)

#chmod 755 $DOCUMENT_ROOT/../admin

exit 0;
