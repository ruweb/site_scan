#!/bin/sh
export PATH='/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/sbin:/usr/local/bin:/opt/local/bin'
for pid in $(pgrep -f sitescan_run.sh); do
    [ "$pid" != "$$" ] && echo "Error: sitescan_run.sh already running ($pid)" && exit 1
done

#rm /home/*/.plugins/site_scan/infected.list #while testing

clamscan=$(which clamscan)
hostname=$(hostname)
#sender="diradmin@$hostname"
sender="admin@$hostname"
fromaddr="$sender"
plugindir="/usr/local/directadmin/plugins/site_scan"
datadir="$plugindir/data"
templdir="$plugindir/scripts/templates"
tmpdir="/tmp/site_scan_plugin"
port=$(grep -i '^ssl_port=' /usr/local/directadmin/conf/directadmin.conf) || port=$(grep -i '^port=' /usr/local/directadmin/conf/directadmin.conf) \
    && port=${port#*=}

[ "$(uname)" = "FreeBSD" ] && chmod="chmod -h" || chmod="chmod"

[ -z "$clamscan" ] && echo "ERROR: clamscan not found" && exit 2

[ ! -d "$plugindir/clamav" ] && echo "Error: $plugindir/clamav/ not found" && exit 2

[ -d "$datadir" ] || mkdir -m755 "$datadir" && chown -h admin:admin "$datadir"

export LANG=en_US.UTF-8
umask 0077

rm -rf "$tmpdir"
mkdir "$tmpdir" && cd "$tmpdir" || exit 3

if [ -e "$datadir/full_scan.log" ] && [ "$(date +%u)" -ne 2 ]; then
    scanlog="$datadir/new_scan.log"
    (cd $datadir && : > new_files.list && date > new_scan.log && chown -h admin:admin new_scan.log)
    [ -e /backup/logs/system/ ] && lastlog=$(find /backup/logs/system/ -mmin -600 -name '*.*.log' | sort | tail -1)
    if [ -n "$lastlog" ]; then
        grep -h '\] >f' "${lastlog%%.*}".*.log | awk '{$1=$2=$3=$4=""; print "/"substr($0,5)}' | grep '^/home/[a-z0-9]*/' \
            | grep -Ev '^/home/[a-z0-9]*/(imap/|Maildir/\.|[a-z_]*backups/|domains/.*/logs/.*\.tar\.gz)' > "$datadir/new_files.list"
        touch -r "$lastlog" "$datadir/lastrun.new"
    else
        touch "$datadir/lastrun.new"
        find /home/ -type f \( -newer "$datadir/lastrun" -or -cnewer "$datadir/lastrun" \) -print | grep '^/home/[a-z0-9]*/' \
            | grep -Ev '^/home/[a-z0-9]*/(imap/|Maildir/\.|[a-z_]*backups/|domains/.*/logs/.*\.tar\.gz)' > "$datadir/new_files.list"
    fi
    for file in /home/*/.plugins/site_scan/infected.list; do
        [ -s "$file" ] && perl -pe 's/^(.*): \S+ FOUND$/$1/' "$file" >> "$datadir/new_files.list"
    done
    sort -uo "$datadir/new_files.list" "$datadir/new_files.list"
    nice -20 "$clamscan" --quiet -id "$plugindir/clamav" --scan-archive=no --pcre-recmatch-limit=5000 \
        -l "$scanlog" -f "$datadir/new_files.list" 2>"$datadir/new_scan.errors"
else
    scanlog="$datadir/full_scan.log"
    date > "$scanlog" && chown -h admin:admin "$scanlog"
    touch "$datadir/lastrun.new"
    nice -20 "$clamscan" --quiet -id "$plugindir/clamav" --exclude-dir=^/home/[a-z0-9]*/imap/ --exclude='^/home/[a-z0-9]*/domains/.*/logs/.*\.tar\.gz' \
        --exclude='^/home/[a-z0-9]*/[a-z_]*backups/.*\.tar\.gz' --exclude-dir='^/home/mysql/' --exclude-dir='/home/[a-z0-9]*/Maildir/\..*/' \
        --scan-archive=no --pcre-recmatch-limit=5000 -l "$scanlog" -r /home/ 2>"$datadir/full_scan.errors"
#? --exclude-dir='/Maildir/(\.Junk|(INBOX)?\.(spam|Trash))/'
fi

grep '^/home/' "$scanlog" | sort > full.tmp

if [ -s "full.tmp" ]; then
    for user in $(ls /usr/local/directadmin/data/users/); do
        grep -q "^$user:" /etc/passwd || continue
        grep "^/home/$user/" full.tmp | grep -vE '^/home/[^/]*/domains/[^/]*/stats/(awstats[^/:]*|webalizer[^/:]*|[^/:]*\.html):' > new.tmp
        [ -s new.tmp ] || continue
        userdir="/home/$user/.plugins/site_scan"
        for file in "/home/$user/.plugins" "$userdir" "$userdir/infected.list"; do
            [ -L "$file" ] && rm "$file"
        done
        [ -e "/home/$user/.plugins" ] || (mkdir -m711 "/home/$user/.plugins" && chown -h "$user:$user" "/home/$user/.plugins")
        [ -e "$userdir" ] || (mkdir -m700 "$userdir" && chown -h "$user:$user" "$userdir")
        [ -e "$userdir/infected.list" ] || (touch "$userdir/infected.list" && chown -h "$user:$user" "$userdir/infected.list")
        sort new.tmp > "$userdir/infected.list"
        : > list.tmp
        [ -e "$userdir/whitelist.txt" ] && whitelist="$userdir/whitelist.txt" || whitelist="/dev/null"
        [ -e "$userdir/autoblock_off" ] && autoblock="" || autoblock="1"
        for file in $(perl -pe 's/^(.*): \S+ FOUND$/$1/' new.tmp | grep -Fxvf "$whitelist"); do
            grep -m1 "^$file: " new.tmp >>list.tmp && [ -n "$autoblock" ] && [ ! -L "$file" ] && $chmod 000 "$file"
        done
        [ -s list.tmp ] || continue
        userconf="/usr/local/directadmin/data/users/$user/user.conf"
        grep -qi '^account=ON' "$userconf" || continue
        name=$(grep -i '^name=' "$userconf") && name=${name#*=}
        email=$(grep -i '^email=' "$userconf") && email=${email#*=}
        email2=${email#*,} && [ "$email" != "$email2" ] && email2=",$email2" || email2=''
        email=${email%%,*}
        skin=$(grep -i '^skin=' "$userconf") && skin=${skin#*=}
        [ "$skin" = "power_ru" ] && lang='ru' || lang=$(grep -i '^language=' "$userconf") && lang=${lang#*=}
        [ -e "$templdir/mail_top.$lang.txt" -a -e "$templdir/mail_bottom.$lang.txt" ] || lang='en'
        "$plugindir/scripts/format_table.pl" "$hostname" "$port" "$skin" <list.tmp >new.tmp
        echo "To: $name <$email>$email2
From: DirectAdmin Site Scanner <$fromaddr>
Subject: WARNING: Virus or malware found inside $user account
MIME-Version: 1.0
Content-Type: text/html; charset=utf-8
" | cat - "$templdir/mail_top.$lang.txt" | ( [ -n "$autoblock" ] && cat - "$templdir/mail_blocked.$lang.txt" || cat - ) \
    | cat - "$templdir/mail_mid.$lang.txt" new.tmp "$templdir/mail_bottom.$lang.txt" | /usr/sbin/sendmail -t -f "$sender"
        : > new.tmp
    done
    count=$(wc -l full.tmp | awk '{print $1}')
    queue="/usr/local/directadmin/data/task.queue.cb"
    printf "action=notify&value=admin&subject=Warning: $count infected files found by clamav&message=" > "$queue" \
        && perl -pe 's#([^a-zA-Z0-9_.!~*()/'\''-])#sprintf("%%%02X", ord($1))#ge' full.tmp >> "$queue" \
        && echo '%0A---%0ASite Scanner Plugin' >> "$queue"
fi

rm -f "$datadir/lastrun"
mv "$datadir/lastrun.new" "$datadir/lastrun"

cd && rm -rf "$tmpdir"
