#!/bin/sh
export PATH='/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/sbin:/usr/local/bin:/opt/local/bin'
export LANG=en_US.UTF-8
umask 0077

[ -n $1 ] || exit 1

homedir=$1
userdir="$homedir/.plugins/site_scan"
plugindir="/usr/local/directadmin/plugins/site_scan"
clamscan=$(which clamscan)

cd "$homedir"

grep "^$homedir/" "$userdir/infected.list" | awk -F: 'sub(FS $NF,x)' \
    | "$clamscan" --pcre-recmatch-limit=5000 -d "$plugindir/clamav" -f /dev/stdin 2>/dev/null > "$userdir/rescan.log"
if grep -q '^Scanned files: ' "$userdir/rescan.log" && ! grep -q '^Scanned files: 0' "$userdir/rescan.log"; then
    grep "^$homedir/" "$userdir/rescan.log" > "$userdir/infected.list" && mv "$userdir/lastrun.new" "$userdir/lastrun"
fi
