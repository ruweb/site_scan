#!/usr/bin/env perl
use strict;
use warnings;
use MIME::Base64;

my $hostname = $ARGV[0];
my $port = $ARGV[1];
my $skin = $ARGV[2];

while (<STDIN>) {
    chomp;
    if (m#^/home/[a-z0-9]+/(.+): (\S+?)(\.UNOFFICIAL)? FOUND$#) {
            my $name=$1;
            my $virus=$2;
            my $base64name=encode_base64($name);
            $name =~ s/([&<>"'])/sprintf('&#%u;',ord($1))/ge;
            print "<tr>
<td style='word-wrap:break-word'>$name</td>
<td style='word-wrap:break-word'>$virus</td>
<td align=center><a href='https://$hostname:$port/CMD_PLUGINS/site_scan/?whitelist=$base64name&skin=$skin' target=_blank>Unblock</a></td>
</tr>";
    }
}
