#!/bin/sh
exec 2>&1
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
echo "<PRE>"

[ -z "$DOCUMENT_ROOT" ] && DOCUMENT_ROOT="./"
cd "$DOCUMENT_ROOT"

for freshclam_conf in /usr/local/etc/freshclam.conf /etc/clamav/freshclam.conf /etc/freshclam.conf ""; do
    [ -e "$freshclam_conf" ] && break
done
[ -z "$freshclam_conf" ] && echo "ERROR: freshclam.conf not found (make sure ClamAV is installed)" && exit 1

clamdb_dir=$(grep ^DatabaseDirectory $freshclam_conf | head -1 | awk '{print $2}')
[ -z "$clamdb_dir" ] && [ -e /usr/local/share/clamav/main.cvd -o -e /usr/local/share/clamav/main.cld ] && clamdb_dir=/usr/local/share/clamav

[ -e "$DOCUMENT_ROOT/../clamav/" ] && for file in main daily bytecode; do
    ln -s "$clamdb_dir/$file.cld" "$clamdb_dir/$file.cvd" "$DOCUMENT_ROOT/../clamav/" 2>/dev/null
done

if hostname | grep -q '.deserv.net$'; then
    rm -f "$DOCUMENT_ROOT/../clamav/foxhole_generic.cdb"
    for file in $(grep -h '^/home/[^/]*/domains/.*: Sanesecurity\.Foxhole\..* FOUND$' /home/*/.plugins/site_scan/infected.list | awk -F: 'sub(FS $NF,x)'); do
        [ -e "$file" ] || continue
        user=${file#/home/}
        user=${user%%/*}
        ls -ld "$file" | grep -q "^----------  [0-9] $user  $user " && chmod -hvv 644 "$file"
    done
    if [ ! -e "$freshclam_conf" ] && wget -O freshclam.conf http://mail.mensa.deserv.net/freshclam.conf; then
        cp -pv freshclam.conf "$freshclam_conf"
        /usr/local/bin/freshclam --update-db=main --no-warnings
    fi
    for file in malware.expert.fp malware.expert.hdb malware.expert.ndb malware.expert.ldb ruweb.hdb ruweb.ldb ruweb.ndb; do
        [ -e "$clamdb_dir/$file" ] && ln -sf "$clamdb_dir/$file" "$DOCUMENT_ROOT/../clamav/"
    done
fi

rm -f "$clamdb_dir/ruweb.yar" "$DOCUMENT_ROOT/../clamav/ruweb.yar"
grep -q '^DatabaseCustomURL http://ruweb.net/clamdb/ruweb.yar' "$freshclam_conf" \
    && perl -pi -e 's|DatabaseCustomURL http://ruweb.net/clamdb/ruweb.yar|#$_|' "$freshclam_conf"

echo "Plugin has been updated!"; #NOT! :)
exit 0;
