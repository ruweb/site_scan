<?
class txtobj implements ArrayAccess {
    private $container = array();

    public function __construct() {
        $this->container = array();
    }

    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    public function offsetExists($offset) {
        return isset($this->container[$offset]);
    }

    public function offsetUnset($offset) {
        unset($this->container[$offset]);
    }

    public function offsetGet($offset) {
        return strlen($this->container[$offset]) ? $this->container[$offset] : $offset;
    }
}

$lang='ru';
#$lang='en';

$_GET = Array();
$QUERY_STRING=getenv('QUERY_STRING');
if ($QUERY_STRING != "")
{
            parse_str(html_entity_decode($QUERY_STRING), $get_array);
            foreach ($get_array as $key => $value)
            {
                        $_GET[urldecode($key)] = urldecode($value);
            }
}

$_POST = Array();
$POST_STRING=getenv('POST');
if ($POST_STRING != "")
{
            $post_array=parseQueryString(html_entity_decode($POST_STRING));
            foreach ($post_array as $key => $value)
            {
                        $_POST[urldecode($key)] = urldecode($value);
            }
}

$params=$_GET+$_POST;

parse_str(str_replace('; ','&',$_SERVER['HTTP_COOKIE']),$_COOKIE);

if (!$params['skin'] && $_COOKIE['skin']) $params['skin']=$_COOKIE['skin'];
if (!$params['charset'] && $_COOKIE['charset']) $params['charset']=$_COOKIE['charset'];

if (!$txt) $txt= new txtobj;
@include_once($_ENV['DOCUMENT_ROOT'].'/lang/default.php');

if ($params['skin']=='power_ru') {
    $_ENV['LANGUAGE']='ru';
    if (!$params['charset']) $params['charset']='windows-1251';
} elseif (!$_ENV['LANGUAGE'] && $_GET['lang']) $_ENV['LANGUAGE']=$_GET['lang'];

if ($_ENV['LANGUAGE'] && file_exists("{$_ENV['DOCUMENT_ROOT']}/lang/lang.{$_ENV['LANGUAGE']}.php")) {
    include("{$_ENV['DOCUMENT_ROOT']}/lang/lang.{$_ENV['LANGUAGE']}.php");
    $lang=$_ENV['LANGUAGE'];
}

if ($lang=='ru' && $CHARSET!='utf-8' && $params['skin']!='power_ru' && $params['charset']!='windows-1251' && (extension_loaded('iconv') || @dl('iconv.so'))) {
    iconv_set_encoding("internal_encoding", "WINDOWS-1251");
    iconv_set_encoding("output_encoding", "UTF-8");
    ob_start("ob_iconv_handler");
} elseif ($CHARSET AND $params['charset'] AND strcasecmp($params['charset'],$CHARSET)!=0 AND (extension_loaded('iconv') OR @dl('iconv.so'))) {
    iconv_set_encoding("internal_encoding", strtoupper($CHARSET));
    iconv_set_encoding("output_encoding", strtoupper($params['charset']));
    ob_start("ob_iconv_handler");
}
//### !!! DO NOT START OUTPUT BEFORE ob_start() !!!

if ($_COOKIE['skin']!=$params['skin']) echo '<script>document.cookie="skin='.rawurlencode($params['skin']).';expires='.(60*60*24*365)."\";</script>\n";
if ($_COOKIE['charset']!=$params['charset']) echo '<script>document.cookie="charset='.rawurlencode($params['charset']).';expires='.(60*60*24*365)."\";</script>\n";

#if (!$params['domain']) {message("Error occured", "Domain name not specified"); exit;}
if (!$params['domain']) $params['domain']=$_ENV['SESSION_SELECTED_DOMAIN'];

if (!extension_loaded('pcre')) dl('pcre.so');

$USER=$_SERVER['USER'];

function message($text,$details='',$exit=true){
        GLOBAL $txt;
	echo '
<p><table width=50% height=100 cellspacing=0 cellpadding=5>
    <tr>
      <td height="50%" valign="middle" align="center">
        <p align="center">'.$text.'</p>
      </td>
    </tr>
    <tr>
    	<td height=1 valign="middle" align="center">
    		<table width = 50%>
    			<tr><td bgcolor="#C0C0C0"> </td></tr>
    		</table>
    	</td>
    </tr>
    <tr>
      <td height="50%" valign="middle" align="center">';
	if ($details=='back') echo '<p><a href="javascript:history.back()">'.($txt['Back']?$txt['Back']:'Back').'</a></p>';
	elseif ($details) echo '
        <p align="center"><b>Details</b></p>
        <p align="center">'.$details.'</p>';
	echo '
      </td>
    </tr>
</table>
</p>
';
	if ($exit) {
		echo $GLOBALS['PAGE_FOOTER'];
		exit;
	}
}


function mkdir_r($path,$mode=FALSE){
    $path=rtrim($path,'/');
    if (file_exists($path) && is_dir($path)) return TRUE;
    else {
	$dir = substr($path,0,strrpos($path, '/'));
	if (mkdir_r($dir,$mode)) return ($mode==FALSE?mkdir($path):mkdir($path,$mode));
	else return FALSE;
    }
}

function delTree($dir){
    $files = array_diff(scandir($dir), array('.','..'));
    foreach ($files as $file) {
	(is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
    }
    return rmdir($dir);
}

function options($options,$selected='',$firstempty=' !@#',$skip=' !@#'){
	if ($firstempty!=' !@#' && ($firtempty || $firstempty==$selected)) $result='<option></option>';
	while (list($key,$val)=each($options)) if ($skip!="$key")
		$result.="\n<option value='$key'".($selected=="$key"?' selected':'').">$val</option>";
	return $result;
}

function passwd($len=8){
    $pass='';
    $arr=explode(",","A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9");
    for ($i=1;$i<=$len;$i++) {
	$pass.=$arr[rand(0,count($arr)-1)];
    }
    return $pass;
}

function parseQueryString($str) {
    $op = array();
    $pairs = explode("&", $str);
    foreach ($pairs as $pair) {
        list($k, $v) = array_map("urldecode", explode("=", $pair)); 
        $op[$k] = $v;
    }
    return $op;
}

#http://php.net/manual/ru/function.date-default-timezone-set.php#113864
function setTimezone($default) {
    $timezone = "";
    
    // On many systems (Mac, for instance) "/etc/localtime" is a symlink
    // to the file with the timezone info
    if (is_link("/etc/localtime")) {
        
        // If it is, that file's name is actually the "Olsen" format timezone
        $filename = readlink("/etc/localtime");
        
        $pos = strpos($filename, "zoneinfo");
        if ($pos) {
            // When it is, it's in the "/usr/share/zoneinfo/" folder
            $timezone = substr($filename, $pos + strlen("zoneinfo/"));
        } else {
            // If not, bail
            $timezone = $default;
        }
    }
    elseif (file_exists("/etc/timezone")){
        // On other systems, like Ubuntu, there's file with the Olsen time
        // right inside it.
        $timezone = @file_get_contents("/etc/timezone");
    }
    elseif ($tz_short=strtolower(trim(`/bin/date "+%Z"`))){
        $timezone_abbreviations = DateTimeZone::listAbbreviations();
        $timezone=$timezone_abbreviations[$tz_short][0]['timezone_id'];
    }
    
    if (!strlen($timezone)) $timezone = $default;
    date_default_timezone_set($timezone);
}

function getAccountData() {
    if ($str = apiRequest('CMD_API_SHOW_USER_CONFIG')) parse_str($str,$result);
    else return false;
    return $result;
}

function apiRequest($cmd,$params=[],$method='GET') {
    $port=$_SERVER['SERVER_PORT'];
    if ($_SERVER['SSL'])
        if (extension_loaded('openssl') OR @dl('openssl.so')) $proto="https";
        else { $proto="http"; $port=2222; }
    else $proto="http";
    $data = http_build_query($params);
    $context_options = array (
        'http' => array (
            'method' => $method,
            'content' => ($method=='POST'?$data:null),
            'header'=> "Cookie: session={$_SERVER['SESSION_ID']}; key={$_SERVER['SESSION_KEY']}",
            ),
        'ssl' => array (
            'verify_peer' => false,
            'verify_peer_name' => false,
            ),
        );
    $context = stream_context_create($context_options);
    if ($method=='GET' AND $params) $data="?$data";
    else $data='';
    if ($str = file_get_contents("$proto://127.0.0.1:$port/$cmd$data", false, $context)) return $str;
    else return false;
}


$PAGE_FOOTER="\n</p>\n<br>\n$PLUGIN_NAME &copy; $PLUGIN_YEAR <a href=https://ruweb.net target=_blank>RuWeb.net</a></center>";
if (strpos($params['skin'],'power_')===0) $PAGE_FOOTER.='
<table border="0" width="750" cellspacing="5" cellpadding="0">
    <tr>
      <td class=bars height="20" valign="top" bgcolor="#C0C0C0">
        <table width=100% cellspacing=0 cellpadding=3>
            <tr>
            <td width=80% align=left>
                <a class=tree style="font-size:10pt;" href="/">'.($txt['Home']?$txt['Home']:'Home').'</a>
                &raquo; <a class=tree href="/CMD_SHOW_DOMAIN?domain='.$_ENV['SESSION_SELECTED_DOMAIN'].'">'.$_ENV['SESSION_SELECTED_DOMAIN'].'</a>
                &raquo; <a class=tree href="?domain='.$params['domain'].'">'.($txt['PLUGIN_TITLE']?$txt['PLUGIN_TITLE']:$PLUGIN_NAME).'</a>
            </td>
            <td width=20% align=right>
                <p>
                    <a class=tree style="font-size:10pt; cursor:help;" target="help" href="http://www.site-helper.com/">Help</a>
                </p>
            </td>
            </tr>
        </table>
      </td>
    </tr>
  </table>
</div>

</body>

</html>
<!--';

echo "<center><br><a href='?domain={$params['domain']}'><b>".($txt['PLUGIN_TITLE']?$txt['PLUGIN_TITLE']:$PLUGIN_NAME)."</b></a><br>\n<p>\n";
