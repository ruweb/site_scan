# Site Scanner (DirectAdmin Plugin)#

Free open-source plugin for daily ClamAV run

### Details ###

Download & install from https://plugins.ruweb.net/site_scan.tar.gz

During installation daily cron job will be added to /etc/crontab:
```
0 4 * * * root /usr/local/directadmin/plugins/site_scan/scripts/sitescan_run.sh
```
Once a week (on Tuesday or on the first run) full /home scan will be performed with clamscan, other days only new files (by mtime/ctime) will be scanned.

By default infected files will be blocked by executing chmod 000. (User can disable auto-blocking feature inside plugin interface in DirectAdmin.)

After every scan list of infected files with brief instructions will be e-mailed to user, also full list of infected files will be reported to admin via DirectAdmin message system.

User can add files to whitelist - whitelisted files will not be blocked and will not be reported to user.

User interface example: http://i.imgur.com/lw3nL6c.png

Russian interface example: https://forum.ruweb.net/viewthread.php?tid=3017

### Note ###

Only signature databases added to /usr/local/directadmin/plugins/site_scan/clamav/ directory will be used during scan.
(Symlinks to default databases will be added there during installation).

It is highly recommended to add Linux Malware Detect signatures to your databases:
```
DatabaseCustomURL http://www.rfxn.com/downloads/rfxn.ndb
DatabaseCustomURL http://www.rfxn.com/downloads/rfxn.hdb
```
We also found Malware Expert signatures quite useful and effective:
```
DatabaseCustomURL http://cdn.malware.expert/malware.expert.ndb
DatabaseCustomURL http://cdn.malware.expert/malware.expert.hdb
DatabaseCustomURL http://cdn.malware.expert/malware.expert.ldb
DatabaseCustomURL http://cdn.malware.expert/malware.expert.fp
```
Add this to your freshclam.conf if you haven't done so yet. (Then execute freshclam and reinstall plugin - symlinks will be added to /usr/local/directadmin/plugins/site_scan/clamav/)

You may want to add our whitelist also (and/or create your own whitelist)
```
DatabaseCustomURL http://ruweb.net/whitelist_ruweb.ign2
```
to skip some False-Positive signatures.

### Discussion ###

https://forum.directadmin.com/showthread.php?t=55080
